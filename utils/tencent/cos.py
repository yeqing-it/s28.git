from qcloud_cos import CosConfig
from qcloud_cos import CosS3Client
from django.conf import settings
from qcloud_cos.cos_exception import CosServiceError
def cretae_bucket(bucket,region="ap-nanjing"):
    """创建桶
    bucket:桶名称
    region：区域
    """
    # secret_id = 'settings.Te'      # 替换为用户的 secretId
    # secret_key = ''      # 替换为用户的 secretKey

    # token = None                # 使用临时密钥需要传入 Token，默认为空，可不填
    # scheme = 'https'            # 指定使用 http/https 协议来访问 COS，默认为 https，可不填
    config = CosConfig(Region=region, SecretId=settings.TENCENT_COS_ID, SecretKey=settings.TENCENT_COS_KEY)
    # 2. 获取客户端对象
    client = CosS3Client(config)
    client.create_bucket(
       Bucket=bucket,
       ACL ='public-read'
    )
    cors_config = {
        'CORSRule': [
            {
                'AllowedOrigin': '*',
                'AllowedMethod': ['GET', 'PUT', 'HEAD', 'POST', 'DELETE'],
                'AllowedHeader': "*",
                'ExposeHeader': "*",
                'MaxAgeSeconds': 500
            }
        ]
    }
    client.put_bucket_cors(
        Bucket=bucket,
        CORSConfiguration=cors_config
    )


def upload_file(bucket,region,file_object,key):
    # secret_id = 'AKIDXy931i8krwoOkpOGelbARTD5gNUKmFo6'  # 替换为用户的 secretId
    # secret_key = 'FI5d0b0D9pIVRAe4aaLwIODl26tUpDLE'  # 替换为用户的 secretKey
    # region = 'ap-nanjing'  # 替换为用户的 Region
    # token = None                # 使用临时密钥需要传入 Token，默认为空，可不填
    # scheme = 'https'            # 指定使用 http/https 协议来访问 COS，默认为 https，可不填
    config = CosConfig(Region=region, SecretId=settings.TENCENT_COS_ID, SecretKey=settings.TENCENT_COS_KEY)
    client = CosS3Client(config)
    response = client.upload_file_from_buffer(
        Bucket=bucket,
        Body=file_object,  # 文件对象
        Key=key
    )
    return "https://{}.cos.{}.myqcloud.com/{}".format(bucket, region, key)

def delete_file(bucket,region,key):
    # secret_id = 'AKIDXy931i8krwoOkpOGelbARTD5gNUKmFo6'  # 替换为用户的 secretId
    # secret_key = 'FI5d0b0D9pIVRAe4aaLwIODl26tUpDLE'  # 替换为用户的 secretKey
    # region = 'ap-nanjing'  # 替换为用户的 Region
    # token = None                # 使用临时密钥需要传入 Token，默认为空，可不填
    # scheme = 'https'            # 指定使用 http/https 协议来访问 COS，默认为 https，可不填
    config = CosConfig(Region=region, SecretId=settings.TENCENT_COS_ID, SecretKey=settings.TENCENT_COS_KEY)
    client = CosS3Client(config)
    client.delete_object(
        Bucket=bucket,
        Key=key
    )
def check_file(bucket,region,key):
    config = CosConfig(Region=region, SecretId=settings.TENCENT_COS_ID, SecretKey=settings.TENCENT_COS_KEY)
    client = CosS3Client(config)
    data = client.head_object(
        Bucket=bucket,
        Key=key
    )
    return data
def delete_file_list(bucket,region,key_list):
    # secret_id = 'AKIDXy931i8krwoOkpOGelbARTD5gNUKmFo6'  # 替换为用户的 secretId
    # secret_key = 'FI5d0b0D9pIVRAe4aaLwIODl26tUpDLE'  # 替换为用户的 secretKey
    # region = 'ap-nanjing'  # 替换为用户的 Region
    # token = None                # 使用临时密钥需要传入 Token，默认为空，可不填
    # scheme = 'https'            # 指定使用 http/https 协议来访问 COS，默认为 https，可不填
    config = CosConfig(Region=region, SecretId=settings.TENCENT_COS_ID, SecretKey=settings.TENCENT_COS_KEY)
    client = CosS3Client(config)
    objects = {
        "Quiet":"true",
        "Object": key_list
    }
    client.delete_object(
        Bucket=bucket,
        Delete=objects

    )
def credential(bucket, region, Sts=None,):

    """ 获取cos上传临时凭证 """

    from sts.sts import Sts



    config = {
        # 临时密钥有效时长，单位是秒（30分钟=1800秒）

        'duration_seconds': 5,
        # 固定密钥 id
        'secret_id': settings.TENCENT_COS_ID,
        # 固定密钥 key
        'secret_key': settings.TENCENT_COS_KEY,
        # 换成你的 bucket
        'bucket': bucket,
        # 换成 bucket 所在地区
        'region': region,
        # 这里改成允许的路径前缀，可以根据自己网站的用户登录态判断允许上传的具体路径
        # 例子： a.jpg 或者 a/* 或者 * (使用通配符*存在重大安全风险, 请谨慎评估使用)
        'allow_prefix': '*',
        # 密钥的权限列表。简单上传和分片需要以下的权限，其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
        'allow_actions': [
            # "name/cos:PutObject",
            # 'name/cos:PostObject',
            # 'name/cos:DeleteObject',
            # "name/cos:UploadPart",
            # "name/cos:UploadPartCopy",
            # "name/cos:CompleteMultipartUpload",
            # "name/cos:AbortMultipartUpload",
            "*",
        ],

    }

    sts = Sts(config)
    result_dict = sts.get_credential()
    return result_dict
def delete_bucket(bucket,region):
    """ 删除桶 """
    # 先删除桶中的文件
    # 先删除桶中的碎片
    # 删除桶
    config = CosConfig(Region=region, SecretId=settings.TENCENT_COS_ID, SecretKey=settings.TENCENT_COS_KEY)
    client = CosS3Client(config)
    # 找到桶中所有文件
    try:
        while True:
            part_objects = client.list_objects(bucket)

            # 已经删除完毕，获取不到值
            contents = part_objects.get('Contents')
            if not contents:
                break
            # 批量删除
            objects = {
                "Quiet": "true",
                "Object": [{'Key':item['Key']}for item in contents]
            }
            client.delete_objects(bucket,objects)
            if part_objects['IsTruncated'] == "false":
                break
        # 找到碎片 删除
        while True:
            part_uploads = client.list_multipart_uploads(bucket)
            uploads = part_uploads.get('Upload')
            if not uploads:
                break
            for item in uploads:
                client.abort_multipart_upload(bucket,item['Key'],item['UploadId'])
            if part_uploads['IsTruncated'] == "false":
                break
        client.delete_bucket(bucket)
    except CosServiceError as e:
        pass
