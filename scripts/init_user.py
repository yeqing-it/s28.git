
import django
import os
import sys
base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(base_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE",'study.settings')
django.setup()  # os.environ['DJANGO_SETTINGS_MODULE']

from web import models
# 往数据库添加数据；链接数据库操作关闭链接
models.UserInfo.objects.create(username='张三',email='zhangsan@live.com',mobile_phone='18888888888',password='123456789')