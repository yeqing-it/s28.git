from django.template import Library
from web import models
from django.urls import reverse
register = Library()

@register.simple_tag
def string_just(num):
    if num < 100:
        num = str(num).rjust(3,"0")  # 转化的字符串不足3位就前面补0
    return "#{}".format(num)