from django import forms
from web.forms.bootstrap import BootStapForm
from web import models
class IssusesModelForm(BootStapForm,forms.ModelForm):
    class Meta:
        model = models.Issues
        exclude = ['project','creator','latest_update_datetime']
        widgets = {
            "assign":forms.Select(attrs={'class':"selectpicker","data-live-search":"true"}),
            "attention":forms.SelectMultiple(attrs={'class':"selectpicker","data-live-search":"true","data-actions-box":"true"}),
            "parent": forms.Select(
                attrs={'class': "selectpicker", "data-live-search": "true"})
        }

    def __init__(self,request,*args,**kwargs):
        super().__init__(*args,**kwargs)

        # 处理数据初始化
        # 获取当前项目的所有问题类型
        self.fields['issues_type'].choices = models.IssuesType.objects.filter(
            project=request.tracer.project).values_list('id','title')
        # 获取当前项目所有模块
        module_list = [("","没有选中任何项"),]
        module_object_list = models.Module.objects.filter(
            project=request.tracer.project).values_list('id', 'title')
        module_list.extend(module_object_list)
        self.fields['module'].choices = module_list
        # 指派和关注者
        # 数据库中找到当前项目的参与者和创建者
        total_user_list = [(request.tracer.project.creator_id,request.tracer.project.creator.username),]
        project_user_list = models.ProjectUser.objects.filter(project=request.tracer.project).values_list('user_id','user__username')

        self.fields['assign'].choices = [("","没有选中任何项")] + total_user_list
        self.fields['attention'].choices = total_user_list

        # 当前项目已创建的问题
        parent_object_list = models.Issues.objects.filter(project=request.tracer.project).values_list("id","subject")
        parent_list = [("","没有选中任何项")]
        parent_list.extend(parent_object_list )
        self.fields['parent'].choices = parent_list

class IssueReplyModelForm(forms.ModelForm):
    class Meta:
        model = models.IssuesReply
        fields = ['content','reply']

class InviteModelForm(BootStapForm,forms.ModelForm):
    class Meta:
        model = models.ProjectInvite
        fields = ['period','count']