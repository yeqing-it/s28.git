from django import forms
from web.forms.bootstrap import BootStapForm
from web import models
from django.core.exceptions import ValidationError
from web.forms.widgets import ColorRadioSelect


class ProjectModelForm(BootStapForm,forms.ModelForm):
    bootstrap_class_exclude = ['color']
    # desc = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = models.Project
        fields = ['name','color','desc']
        widgets = {
            'desc':forms.Textarea,
            'color':ColorRadioSelect(attrs={'class':'color-radio'})

        }

    def __init__(self,request,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.request = request
    def clean_name(self):
        """ 项目校验 """
        # 1.当前用户是已创建过此项目
        name = self.cleaned_data['name']

        exists = models.Project.objects.filter(name=name,creator=self.request.tracer.user).exists()
        if exists:
            raise  ValidationError('项目名已存在')

        # 2.当前用户是否还有额度进行创建项目？
        # 最多创建多少个项目

        # print(self.request.tracer.price_policy.project_num)
        # 现在已经创建多少项目？
        count = models.Project.objects.filter(creator=self.request.tracer.user).count()
        print(count,1111111111)
        print(self.request.tracer.price_policy,555)
        print(self.request.tracer.price_policy.project_num,2222222)
        if count >= self.request.tracer.price_policy.project_num:
            raise ValidationError('项目个数超限，请购买套餐')
        return name